<html>
<head>
<title>PHP Multiple Choice Questions and Answers</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="score.php">
<link rel="stylesheet" href="question.css">
</head>
<body>

    <?php

        if (isset($_POST['save-session']))
        {
            // Lưu Session
            $_COOKIE['q1'] = $_POST['answer1'];
            $_COOKIE['q2'] = $_POST['answer2'];
            $_COOKIE['q3'] = $_POST['answer3'];
        }

        $grade=0;
        if (isset($_COOKIE['q1']) && $_COOKIE['q1'] == "a")
        {
            $grade=$grade+1;
            echo 'Answer1: ' . $_COOKIE['q1'];
        }
        if (isset($_COOKIE['q2']) && $_COOKIE['q2'] == "b")
        {
            $grade=$grade+1;
            echo 'Answer1: ' . $_COOKIE['q2'];
        }
        if (isset($_COOKIE['q3']) && $_COOKIE['q3'] == "a")
        {
            $grade=$grade+1;
            echo 'Answer1: ' . $_COOKIE['q3'];
        }

        // echo "/n";
        echo $grade;

        setcookie('grade', $grade, time() + 3600);
        setcookie('q4', $_POST["answer4"], time() + 3600);
        setcookie('q5', $_POST["answer5"], time() + 3600);
        setcookie('q6', $_POST["answer6"], time() + 3600);

    ?>

    <div class="container">
    <form action="score.php" method="post">

    <div class="form-group"> 
        <h3>Ques1 : Who is the father of PHP? </h3>
        <ol>
            <li>
                <input type="radio" name="answer4" value="1" />Rasmus Lerdorf
            </li>
            <li>
                <input type="radio" name="answer4" value="2" />Larry Wall
            </li>
            <li>
                <input type="radio" name="answer4" value="3" />Zeev Suraski
            </li>
        </ol>
    </div>
    <br/>
    <div class="form-group"> 
        <h3>Ques2 : Which of the functions is used to sort an array in descending order?</h3>
        <ol>
        <li>
            <input type="radio" name="answer5" value="1" />sort()
        </li>
        <li>
            <input type="radio" name="answer5" value="2" />asort()
        </li>
        <li>
            <input type="radio" name="answer5" value="3" />rsort()
        </li>
        </ol>
    </div>
    <br/>
    <div class="form-group"> 
        <h3>Ques3 : Which version of PHP introduced the instanceof keyword?</h3>
        <ol>
            <li>
                <input type="radio" name="answer6" value="1" />PHP 4 
            </li>
            <li>
                <input type="radio" name="answer6" value="2" />PHP 5
            </li>
            <li>
                <input type="radio" name="answer6" value="3" />PHP 6
            </li>
        </ol>
    </div>
    <div class="form-group">
    <input type="submit" value="Submit" name="save-session" class="btn btn-primary"/>
    </div>
    </form>
    </div>
    
</body>
</html>