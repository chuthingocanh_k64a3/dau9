<html>
<head>
<title>PHP Multiple Choice Questions and Answers</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="submit.php">
<link rel="stylesheet" href="question.css">
</head>
<body>

    <?php
        setcookie('q1', $_POST["answer1"], time() + 3600, "path=/score");
        setcookie('q2', $_POST["answer2"], time() + 3600, "path=/score");
        setcookie('q3', $_POST["answer3"], time() + 3600, "path=/score");

        
    ?>

    <div class="container">
    <form action="page2.php" method="post">

    <div class="form-group"> 
        <h3>Ques1 : Who is the father of PHP? </h3>
        <ol>
            <li>
                <input type="radio" name="answer1" value="a" />Rasmus Lerdorf
            </li>
            <li>
                <input type="radio" name="answer1" value="b" />Larry Wall
            </li>
            <li>
                <input type="radio" name="answer1" value="c" />Zeev Suraski
            </li>
        </ol>
    </div>
    <br/>
    <div class="form-group"> 
        <h3>Ques2 : Which of the functions is used to sort an array in descending order?</h3>
        <ol>
        <li>
            <input type="radio" name="answer2" value="a" />sort()
        </li>
        <li>
            <input type="radio" name="answer2" value="b" />asort()
        </li>
        <li>
            <input type="radio" name="answer2" value="c" />rsort()
        </li>
        </ol>
    </div>
    <br/>
    <div class="form-group"> 
        <h3>Ques3 : Which version of PHP introduced the instanceof keyword?</h3>
        <ol>
            <li>
                <input type="radio" name="answer3" value="a" />PHP 4 
            </li>
            <li>
                <input type="radio" name="answer3" value="b" />PHP 5
            </li>
            <li>
                <input type="radio" name="answer3" value="c" />PHP 6
            </li>
        </ol>
    </div>
    <div class="form-group">
    <input type="submit" value="Next" name="save-session" class="btn btn-primary"/>
    </div>
    </form>
    </div>

</body>
</html>